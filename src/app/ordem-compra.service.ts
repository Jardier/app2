import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http'; 
import { Observable } from 'rxjs/internal/Observable';

import { Pedido } from './shared/pedido.model';
import { URL_API } from './app.api';

@Injectable()
export class OrdemCompraService {

    constructor(private http: Http) {

    }
    public efetivarCompra(pedido : Pedido) : Observable<number> {
        let headres : Headers = new Headers();
        headres.append('Content-type', 'application/json');
        
        return this.http.post(
            `${URL_API}/pedidos`,
            JSON.stringify(pedido),
            new RequestOptions({headers : headres})
        )
        .retry(10)
        .map((resposta : any) =>
            resposta.json().id
        )
       
    }
}