import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { OfertasService } from '../ofertas.service';
import { CarrinhoService } from '../carrinho.service';

import { Oferta } from '../shared/oferta.model';
import { ItemCarrinho } from '../shared/item-carrinho.model';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.css'],
  providers: [OfertasService]
})
export class OfertaComponent implements OnInit, OnDestroy {

  public oferta : Oferta;

  constructor(private route : ActivatedRoute ,
              private ofertasService : OfertasService,
              private carrinhoService : CarrinhoService
              ) { }

  ngOnInit() {

    this.route.params.subscribe((parametros : Params) => {
      this.ofertasService.getOfertaPorId(parametros.id)
                         .then((oferta : Oferta) => {
                           this.oferta = oferta;                         
                         });

    });

    this.carrinhoService.exibirItens();

  }

  ngOnDestroy() {
   
  }

  public adicionarItemCarrinho() : void {
    let itemCarrinho : ItemCarrinho = new ItemCarrinho(
      this.oferta.id,
      this.oferta.imagens[0],
      this.oferta.titulo,
      this.oferta.descricao_oferta,
      this.oferta.valor,
      1
    )
    this.carrinhoService.incluirItem(itemCarrinho);
    this.carrinhoService.exibirItens();
  }

}
