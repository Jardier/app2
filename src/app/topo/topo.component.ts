import { Component, OnInit } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Subscribable, Subject, Observable } from 'rxjs';
import { Oferta } from '../shared/oferta.model';

import '../util/rxjs-extensions';

@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.css'],
  providers: [OfertasService]
})
export class TopoComponent implements OnInit {

  private subjectPesquisa : Subject<string> = new Subject<string>();

  public ofertas : Observable<Oferta[]>;
 

  constructor(private ofertaService : OfertasService) { }

  ngOnInit() {
    this.ofertas = this.subjectPesquisa
        .debounceTime(1000)
        .distinctUntilChanged()
        .switchMap((termoDeBusca : string) => {
          //caso seja passado uma string em branco, devolveremos um Observable vazio
          if(termoDeBusca.trim() === '') {
            return Observable.of<Oferta[]>([]);
          }
          return this.ofertaService.pesquisarOfertas(termoDeBusca);
        })
        .catch((error : any) => {
          console.log('Erro catch: ', error);
          return Observable.of<Oferta[]>([]);
        })
   
        /**
      this.ofertas.subscribe((ofertas : Oferta[]) => {
      //console.log('Resultado: ', ofertas);
      //this.ofertasConsulta = ofertas; //usando o pipe async
    });
     */
  }

  public pequisar(termoDaBusca : string) : void {
    this.subjectPesquisa.next(termoDaBusca);     
  }

  public limpaPesquisa() : void {
    this.subjectPesquisa.next('');
  }
}
