import { ItemCarrinho } from './shared/item-carrinho.model';

class CarrinhoService {

    public itens : ItemCarrinho [] = [];


    public exibirItens() : ItemCarrinho [] {
        return this.itens;
    }

    public incluirItem(itemCarrinho : ItemCarrinho) : void { 
        //verificar se o item a ser iserido existe. Se existir alterar a quantidade
        let itemCarrinhoEncontrado = this.buscarItemNaLista(itemCarrinho);

        if(itemCarrinhoEncontrado) {
            itemCarrinhoEncontrado.quantidade += 1;
        } else {
            this.itens.push(itemCarrinho);    
        }
        
    }

    public totalCarrinhoCompras() : number {
        let total : number = 0;

        this.itens.map((item : ItemCarrinho) => {
            total += (item.valor * item.quantidade);
        });

        return total;
    }

    public adicionarQuantidade(itemCarrinho : ItemCarrinho) : void {
        let itemCarrinhoEncontrado = this.buscarItemNaLista(itemCarrinho);

        if(itemCarrinhoEncontrado) {
            itemCarrinhoEncontrado.quantidade += 1;
        }
    }

    public diminuirQuantidade(itemCarrinho : ItemCarrinho) : void {
        let itemCarrinhoEncontrado = this.buscarItemNaLista(itemCarrinho);

        if(itemCarrinhoEncontrado) {
            itemCarrinhoEncontrado.quantidade -= 1;
            
            //remover o item da lista
            if(itemCarrinhoEncontrado.quantidade === 0) {
                this.itens.splice(this.itens.indexOf(itemCarrinhoEncontrado) , 1);
            }
        }
    }

    public limparCarrinho() : void {
        this.itens = [];
    }

    private buscarItemNaLista(itemCarrinho : ItemCarrinho) : ItemCarrinho {
        return this.itens.find((item : ItemCarrinho) => item.id === itemCarrinho.id);
    }

}
export { CarrinhoService }